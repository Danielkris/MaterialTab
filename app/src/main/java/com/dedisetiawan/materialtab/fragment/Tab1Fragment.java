package com.dedisetiawan.materialtab.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.dedisetiawan.materialtab.R   ;


public class Tab1Fragment extends Fragment {

    private EditText edtPanjang, edtLebar;
    private Button btnHitung;
    private TextView txtLuas;
    public Tab1Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        edtPanjang = (EditText)view.findViewById(R.id.edt_panjang);
        edtLebar = (EditText)view.findViewById(R.id.edt_lebar);
        btnHitung = (Button)view.findViewById(R.id.btn_hitung);
        txtLuas = (TextView)view.findViewById(R.id.txt_luas);
        return view;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String panjang = edtPanjang.getText().toString().trim();
                String lebar = edtLebar.getText().toString().trim();

                double p = Double.parseDouble(panjang);
                double l = Double.parseDouble(lebar);

                double luas = p * l;

                txtLuas.setText("Luas : "+luas);
            }
        });
    }

}